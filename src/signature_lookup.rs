/*
 * Copyright 2018 Jordan Miner
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#[cfg(feature = "strong_hash")]
use std::collections::HashMap;
use std::io::{self, BufReader, Read};

use byteorder::{BE, ByteOrder, ReadBytesExt};
#[cfg(not(feature = "strong_hash"))]
use rustc_hash::FxHashMap;

use crate::{SIG_MAGIC_BLAKE2, Error};

#[cfg(feature = "strong_hash")]
type SigHashMap<K, V> = HashMap<K, V>;
#[cfg(not(feature = "strong_hash"))]
type SigHashMap<K, V> = FxHashMap<K, V>;

#[derive(Clone,Debug)]
pub struct BlockInfo {
    pub strong_sum: Vec<u8>,
    pub block_index: u64,
}

#[derive(Debug)]
pub struct SignatureLookup {
    block_info: SigHashMap<u32, Vec<BlockInfo>>,
    block_size: u32,
    strong_sum_size: u32,
}

impl SignatureLookup {
    pub fn new<R: Read>(file: R) -> Result<Self, Error> {
        let mut file = BufReader::new(file);

        let magic = file.read_u32::<BE>()?;
        if magic != SIG_MAGIC_BLAKE2 {
            return Err(Error::UnsupportedSignatureFormat(magic));
        }
        let block_size = file.read_u32::<BE>()?;
        let strong_sum_size = file.read_u32::<BE>()?;

        let mut block_info = SigHashMap::default();
        let mut block_index = 0;
        let mut weak_sum_buffer = Vec::with_capacity(4);
        loop {
            // This is rather complicated. We can't just call read_u32 on the `Read` because we need
            // to detect when to break out of the loop, while still erroring if 1 to 3 bytes are
            // read (read_u32 forces us to handle 0 bytes the same as 1 to 3 bytes).
            let mut take = file.take(4);
            take.read_to_end(&mut weak_sum_buffer)?;
            file = take.into_inner();
            if weak_sum_buffer.len() == 0 {
                break;
            } else if weak_sum_buffer.len() < 4 {
                return Err(Error::Io(io::Error::from(io::ErrorKind::UnexpectedEof)));
            }
            let weak_sum = BE::read_u32(&weak_sum_buffer);
            weak_sum_buffer.clear();

            let mut strong_sum = vec![0u8; strong_sum_size as usize];
            file.read_exact(&mut strong_sum)?;

            let blocks = block_info.entry(weak_sum).or_insert(vec![]);
            blocks.push(BlockInfo {
                strong_sum,
                block_index,
            });
            block_index += 1;
        }

        Ok(SignatureLookup {
            block_info,
            block_size,
            strong_sum_size,
        })
    }

    pub fn block_size(&self) -> u32 {
        self.block_size
    }

    pub fn strong_sum_size(&self) -> u32 {
        self.strong_sum_size
    }

    pub fn get_blocks(&self, weak_sum: u32) -> Option<&Vec<BlockInfo>> {
        self.block_info.get(&weak_sum)
    }
}
