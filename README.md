
# librsyncr

[![Crates.io](https://img.shields.io/crates/v/librsyncr.svg)](https://crates.io/crates/librsyncr)

[Documentation](https://docs.rs/librsyncr/)

librsyncr is a Rust library to calculate differences between two files without having access to both files on the same system. It is a rewrite of the [librsync C library](https://github.com/librsync/librsync) (without looking at the librsync source code), and the signature and delta files are compatible with that library. The C library is 3562 lines of C (not including comments or vendored Blake2 and MD4 code), but this library is only 753.

## `strong_hash` Feature

The default hash used for looking up blocks from a signature file is not resistant to hash DoS attacks. If you are using this library in a web service to process untrusted signature files, you will likely want to enable the `strong_hash` feature.

## Performance

Testing with Wikiquote dump files (about 38 MB) on a Intel Core-i7-6700K 4.0 GHz processor:

```
# this library
./target/release/examples/rdiff signature ./enwikiquote-20170501-abstract.xml ./enwikiquote-20170501-abstract-sig.xml

85ms (447 MB/s)

# librsync
./rdiff signature ./enwikiquote-20170501-abstract.xml ./enwikiquote-20170501-abstract-sig.xml

77ms (494 MB/s)


# this library
./target/release/examples/rdiff delta ./enwikiquote-20170501-abstract-sig.xml ./enwikiquote-20170701-abstract.xml ./enwikiquote-delta.xml

115ms (330 MB/s)

# librsync
./rdiff delta ./enwikiquote-20170501-abstract-sig.xml ./enwikiquote-20170701-abstract.xml ./enwikiquote-delta.xml

118ms (322 MB/s)


# this library
./target/release/examples/rdiff patch ./enwikiquote-20170501-abstract.xml ./enwikiquote-delta.xml ./enwikiquote-new.xml

67ms (567 MB/s)

# librsync
./rdiff patch ./enwikiquote-20170501-abstract.xml ./enwikiquote-delta.xml ./enwikiquote-new.xml

24ms (1583 MB/s)
```

## File formats

The C librsync library has [documentation on the signature file format](https://github.com/librsync/librsync/blob/131447aa4b8636c8de576e27bc0736f6a5be9bc2/doc/format.md), and the .NET librsync library has [documentation on the delta file format](https://github.com/braddodson/librsync.net/blob/abcda421a74d769b2be986c0b92a41c12a4f96ce/deltaformat.md).

## License

Licensed under the Mozilla Public License Version 2.0.
